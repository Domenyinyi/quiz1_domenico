﻿using Microsoft.AspNetCore.Mvc;
using Quiz1_Domenico.Models;
using Quiz1_Domenico.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Domenico.API
{
    [Route("api/v1/kabel")]
    [ApiController]
    public class KabelApiController : ControllerBase
    {
        private readonly KabelService _serviceMan;

        public KabelApiController(KabelService kabelService)
        {
            this._serviceMan = kabelService;
        }

        [HttpGet("all-kabel",Name ="allKabel")]
        public async Task<ActionResult<List<KabelModel>>> GetAllKabelAsync(int kabelId)
        {
            var kabel = await _serviceMan.GetAsync(kabelId);
            if(kabel == null)
            {
                return BadRequest(null);
            }
            return Ok(kabel);
        }

        [HttpGet("specific-kabel", Name = "specificKabel")]
        public async Task<ActionResult<KabelModel>> GetSpecificKabel(int kabelId)
        {
            var kabel = await _serviceMan.GetSpecificKabelAsync(kabelId);
            if (kabel == null)
            {
                return BadRequest(null);
            }
            return Ok(kabel);
        }

        [HttpPost("insert-kabel", Name = "insertKabel")]
        public async Task<ActionResult<ResponseModel>> InsertNewEmployeeAppAsync([FromBody]KabelModel value)
        {
            var isSuccess = await _serviceMan.InsertKabel(value);

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new kabel."
            });
        }

        [HttpDelete("delete-kabel", Name = "deleteKabel")]
        public async Task<ActionResult<ResponseModel>> DeleteKabelAsync([FromBody] KabelModel value)
        {
            var isSuccess = await _serviceMan.DeleteKabel(value.KabelId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete kabel {value.KabelId}"
            });
        }

        [HttpPut("update-kabel", Name = "updateKabel")]
        public async Task<ActionResult<ResponseModel>> UpdateKabelAsync([FromBody] KabelModel value)
        {
            var isSuccess = await _serviceMan.UpdateKabelAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update kabel {value.KabelName}"
            });
        }
    }
}
