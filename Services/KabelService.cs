﻿using Microsoft.EntityFrameworkCore;
using Quiz1_Domenico.Entities;
using Quiz1_Domenico.Models;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Domenico.Services
{
    public class KabelService
    {
        private readonly KabelDbContext _db;

        public KabelService(KabelDbContext dbContext)
        {
            this._db = dbContext;
        }

        public async Task<bool> InsertKabel(KabelModel kabelModel)
        {
            this._db.Add(new Kabel
            {
                KabelId = kabelModel.KabelId,
                KabelName = kabelModel.KabelName,
                KabelQuantity = kabelModel.KabelQuantity
            });
            await this._db.SaveChangesAsync();
            return true;
        }
        public async Task<List<KabelModel>> GetAllKabelAsync()
        {
            var kabel = await this._db
                .Kabels
                .Select(Q => new KabelModel
                {
                    KabelName = Q.KabelName,
                    KabelQuantity = Q.KabelQuantity,
                    KabelId = Q.KabelId
                })
                .AsNoTracking()
                .ToListAsync();

            return kabel;
        }

        public int GetTotalData()
        {
            var totalKabel = this._db
                .Kabels
                .Count();

            return totalKabel;
        }

        public async Task<List<KabelModel>> GetAsync(int id, int itemPerPage,string filterByName)
        {
            var kabel = await this._db
                .Kabels
                .Where(Q => Q.KabelId == id)
                .Select(Q => new KabelModel
                {
                    KabelName = Q.KabelName,
                    KabelId = Q.KabelId,
                    KabelQuantity = Q.KabelQuantity
                })
                .AsNoTracking()
                .ToListAsync();
            return kabel;
        }

        public async Task<bool> DeleteKabelAsync(int id)
        {
            var kabel = await this._db
                .Kabels
                .Where(Q => Q.KabelId == id)
                .FirstOrDefaultAsync();

            if (kabel == null)
            {
                return false;
            }
            this._db.Remove(kabel);
            await this._db.SaveChangesAsync();



            return true;
        }

        public async Task<KabelModel> GetSpecificKabelAsync(int? id)
        {
            var kabels = await this._db
                .Kabels
                .Where(Q => Q.KabelId == id)
                .Select(Q => new KabelModel
                {

                    KabelId = Q.KabelId,
                    KabelName = Q.KabelName,
                    KabelQuantity = Q.KabelQuantity,
                })
                .FirstOrDefaultAsync();
            return kabels;
        }

        public async Task<bool> UpdateKabelAsync(KabelModel kabel)
        {
            var kabelModel = await this._db.
                Kabels
                .Where(Q => Q.KabelId == kabel.KabelId)
                .FirstOrDefaultAsync();
            if (kabelModel == null)
            {
                return false;

            }

            kabelModel.KabelName = kabel.KabelName;
            kabelModel.KabelQuantity = kabel.KabelQuantity;
            await this._db.SaveChangesAsync();
            return true;
        }
    }
}
