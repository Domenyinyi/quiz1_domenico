﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Domenico.Models
{
    public class KabelModel
    {
        public int KabelId { set; get; }
        [Required]
        [StringLength(7, MinimumLength = 3, ErrorMessage = "Name must be between 3-7 characters")]
        [Display(Name ="Nama")]
        public string KabelName { set; get; }
        [Required]
        [Display(Name = "Jumlah")]
        public int KabelQuantity { set; get; }
    }
}
