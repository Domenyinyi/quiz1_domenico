using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Quiz1_Domenico.Models;
using Quiz1_Domenico.Services;

namespace Quiz1_Domenico.Pages.Kabel
{
    public class ViewKabelModel : PageModel
    {
        private readonly KabelService _serviceMan;

        public ViewKabelModel(KabelService kabelService)
        {
            this._serviceMan = kabelService;
        }

        public List<KabelModel> Kabels { set; get; }

        // For filter 
        [Display(Name = "Search")]
        [BindProperty(SupportsGet = true)]
        public string FilterByName { get; set; }

        public int TotalPage { set; get; }
        [BindProperty(SupportsGet = true)]
        public int PageIndex { set; get; }
        public int ItemPerPage => 3;

        public async Task OnGetAsync()
        {
            if (PageIndex == 0)
            {
                PageIndex = 1;
            }

            // add param filterByName for filtering
            Kabels = await _serviceMan.GetAsync(PageIndex, ItemPerPage, FilterByName);

            var totalKabel = _serviceMan.GetTotalData();

            TotalPage = (int)Math.Ceiling(totalKabel / (double)ItemPerPage);
        }

        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            var isSuccess = await this._serviceMan.DeleteKabelAsync(id);

            if (isSuccess == false)
            {
                return Page();
            }
            return Redirect("/kabel");
        }
    }
}
