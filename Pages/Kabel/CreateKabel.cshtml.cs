using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Quiz1_Domenico.Models;
using Quiz1_Domenico.Services;

namespace Quiz1_Domenico.Pages.Kabel
{
    public class CreateModel : PageModel
    {
        private readonly KabelService _serviceMan;

        public CreateModel(KabelService kabelService)
        {
            this._serviceMan = kabelService;
        }

        [BindProperty]
        public KabelModel NewKabel { set; get; }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            await _serviceMan.InsertKabel(NewKabel);
            return Redirect("/Kabel");
        }
        public void OnGet(int KabelId)
        {
            NewKabel = new KabelModel();
            NewKabel.KabelId = KabelId;
        }
    }
}
